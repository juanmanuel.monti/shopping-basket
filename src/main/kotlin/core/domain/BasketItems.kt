package core.domain

data class BasketItems (val basketItems:MutableList<BasketItem>) {

    fun add(basketItem: BasketItem){
        basketItems.add(basketItem)
    }

    fun getTotalBasketPrice():Double{
        return basketItems
                .map{it.item.price * it.quantity}
                .sum()
    }

    fun hasBooks(booksQuantity:Int):Boolean{
         return basketItems
                 .filter { it.item.category == ItemCategories.BOOKS }
                 .map { it.quantity }
                 .sum() >= booksQuantity
    }

    fun hasBooksAndVideos():Boolean{
        return basketItems
                .map { it.item.category }
                .containsAll(listOf(ItemCategories.BOOKS, ItemCategories.VIDEOS))
    }
}