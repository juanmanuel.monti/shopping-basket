package core.domain

interface ItemRepository {
    fun find(itemName: String):Item?

    fun add(item: Item)
}