package core.domain

interface ShoppingBasketRepository {
    fun find(userId: String): ShoppingBasket?
    fun add(shoppingBasket: ShoppingBasket)
}