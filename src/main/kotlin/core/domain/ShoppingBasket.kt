package core.domain

import java.time.LocalDateTime

data class ShoppingBasket(val userId: String,
                          var basketItems: BasketItems,
                          private val availableDiscounts: Discounts,
                          private val creationDate: LocalDateTime = LocalDateTime.now()) {

    fun addItem(basketItem: BasketItem) {
        basketItems.add(basketItem)
    }

    fun getCreationDate(): LocalDateTime? {
        return creationDate
    }

    fun getFinalPrice(): Double {

        val discount = availableDiscounts.searchForDiscount(basketItems)
        val baseBasketPrice = basketItems.getTotalBasketPrice()

        return if (discount != null) {
            baseBasketPrice - (baseBasketPrice * discount)
        } else {
            baseBasketPrice
        }
    }
}