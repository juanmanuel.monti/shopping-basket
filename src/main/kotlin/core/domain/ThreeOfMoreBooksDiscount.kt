package core.domain

class ThreeOfMoreBooksDiscount:Discount {

    override fun applies(basketItems: BasketItems): Boolean {
        return basketItems.hasBooks(3)
    }

    override fun getDiscountPercentage(basketItems: BasketItems): Double {
        return 0.10
    }
}