package core.domain

interface Discount {

    fun applies(basketItems: BasketItems):Boolean
    fun getDiscountPercentage(basketItems: BasketItems):Double
}