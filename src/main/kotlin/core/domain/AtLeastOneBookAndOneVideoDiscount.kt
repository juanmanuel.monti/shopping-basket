package core.domain

class AtLeastOneBookAndOneVideoDiscount:Discount {

    override fun applies(basketItems: BasketItems): Boolean {
        return basketItems.hasBooksAndVideos()
    }

    override fun getDiscountPercentage(basketItems: BasketItems): Double {
        return 0.20
    }
}
