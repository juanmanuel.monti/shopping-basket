package core.domain

interface ItemStock {

    fun isInStock(item: String,quantity: Int):Boolean
    fun addStock(item: String,quantity:Int)
    fun updateStock(item: String,quantity: Int)
    fun getStock(item: String):Int?
}