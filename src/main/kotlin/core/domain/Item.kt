package core.domain

data class Item(val name: String, val price: Double, val category: ItemCategories) {
}