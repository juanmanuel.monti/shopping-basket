package core.domain

class Discounts(private val catalog: List<Discount>) {

    fun searchForDiscount(basketItems: BasketItems): Double? {
        return catalog
                .filter { it.applies(basketItems) }
                .map { it.getDiscountPercentage(basketItems) }
                .maxOrNull()
    }
}