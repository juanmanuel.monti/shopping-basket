package core.domain

data class BasketItem(val item: Item, val quantity: Int)