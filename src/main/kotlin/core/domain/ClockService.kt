package core.domain

import java.time.LocalDateTime

interface ClockService {
    fun now(): LocalDateTime
}