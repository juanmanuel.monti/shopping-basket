package core.domain

enum class ItemCategories {
    BOOKS,
    VIDEOS
}