package core.action

import core.domain.*

class AddItem(private val shoppingBasketRepository: ShoppingBasketRepository,
              private val clockService: ClockService,
              private val itemRepository: ItemRepository,
              private val availableDiscounts: Discounts) {
    // una accion de dominio nunca deberia recibir por parametro un objeto de dominio

    operator fun invoke(actionData: ActionData) {

        val item = itemRepository.find(actionData.itemName)

        if(isFound(item)) {
            val basketItem = BasketItem(item!!, actionData.quantity)
            val shoppingBasket = shoppingBasketRepository.find(actionData.userId)
            if (isFound(shoppingBasket)) {
                shoppingBasket!!.addItem(basketItem)
            } else {
                val newShoppingBasket = createShoppingBasket(actionData.userId,basketItem,availableDiscounts )
                save(newShoppingBasket)
            }
        }
    }

    private fun isFound(value: Any?)= value!=null

    private fun save(shoppingBasket: ShoppingBasket) {
        shoppingBasketRepository.add(shoppingBasket)
    }

    private fun createShoppingBasket(userId: String, basketItem: BasketItem, availableDiscounts: Discounts): ShoppingBasket {
        val creationDate = clockService.now()
        val basketItems = BasketItems(mutableListOf(basketItem))
        return ShoppingBasket(userId, basketItems, availableDiscounts, creationDate)
    }

    class ActionData(val userId: String, val itemName: String, val quantity: Int)

}