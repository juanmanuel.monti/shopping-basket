package core.action

import core.domain.BasketItems
import core.domain.InsufficientStockException
import core.domain.ItemStock
import core.domain.ShoppingBasket
import core.infrastructure.InMemoryItemStock

class PurchaseItems(private val itemInventory: ItemStock){

    operator fun invoke(shoppingBasket: ShoppingBasket){
        val basketItems = shoppingBasket.basketItems
        if(!stockValidation(basketItems))
            throw InsufficientStockException()
        updateStock(basketItems)
    }

    private fun updateStock(basketItems: BasketItems){
        basketItems.basketItems.map{itemInventory.updateStock(it.item.name,it.quantity)}
    }

    private fun stockValidation(basketItems: BasketItems):Boolean{
        return basketItems.basketItems
                .map{itemInventory.isInStock(it.item.name,it.quantity)}
                .all{ it }
    }
}