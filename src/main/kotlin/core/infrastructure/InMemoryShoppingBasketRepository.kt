package core.infrastructure

import core.domain.ShoppingBasket
import core.domain.ShoppingBasketRepository

class InMemoryShoppingBasketRepository: ShoppingBasketRepository {
    var shoppingBasket = mutableMapOf<String, ShoppingBasket>()
    override fun find(userId: String): ShoppingBasket? {
        return shoppingBasket[userId]
    }

    override fun add(newShoppingBasket: ShoppingBasket) {
        shoppingBasket[newShoppingBasket.userId] = newShoppingBasket
    }
}