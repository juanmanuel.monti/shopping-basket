package core.infrastructure

import core.domain.ItemStock

class InMemoryItemStock:ItemStock {

    var stock = mutableMapOf<String, Int>()

    override fun isInStock(item:String, quantity: Int): Boolean {
        return quantity <= stock[item]!! && stock[item]!! > 0
    }

    override fun addStock(item: String, quantity: Int) {
        stock[item] = quantity
    }

    override fun updateStock(item: String, quantity: Int) {
        stock[item]=stock.getValue(item)-quantity
    }

    override fun getStock(item: String): Int? {
        return stock[item]
    }
}