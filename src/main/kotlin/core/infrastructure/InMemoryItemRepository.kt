package core.infrastructure

import core.domain.Item
import core.domain.ItemRepository

class InMemoryItemRepository:ItemRepository {

    private var itemsRepository = mutableMapOf<String,Item>()

    override fun find(itemName:String): Item? {
        return itemsRepository[itemName]
    }

    override fun add(item: Item) {
        itemsRepository[item.name] = item
    }

}