package core.infrastructure

import core.domain.AtLeastOneBookAndOneVideoDiscount
import core.domain.Discount
import core.domain.ThreeOfMoreBooksDiscount

class DiscountFactory {
    companion object{

        private val threeOfMoreBooksDiscount = ThreeOfMoreBooksDiscount()
        private val oneBookAndOneVideoDiscount = AtLeastOneBookAndOneVideoDiscount()

        fun create():List<Discount>{
            return listOf(threeOfMoreBooksDiscount, oneBookAndOneVideoDiscount)
        }
    }
}