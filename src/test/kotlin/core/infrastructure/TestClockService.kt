package core.infrastructure

import core.domain.ClockService
import java.time.LocalDateTime

class TestClockService(private val expectedNow: LocalDateTime) : ClockService {
    override fun now(): LocalDateTime {
        return expectedNow
    }
}