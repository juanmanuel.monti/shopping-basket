import core.action.AddItem
import core.action.PurchaseItems
import core.domain.*
import core.infrastructure.*
import junit.framework.Assert.*
import org.junit.Before
import org.junit.Test
import java.time.LocalDateTime

class ShoppingBasketTest {

    private lateinit var shoppingBasketRepository: ShoppingBasketRepository
    private lateinit var itemsRepository: InMemoryItemRepository
    private lateinit var itemsStock: InMemoryItemStock
    private lateinit var clockService: ClockService
    private lateinit var addItem: AddItem
    private lateinit var purchaseItems: PurchaseItems
    private lateinit var availableDiscount : Discounts

    private val breakingBadName = "Breaking Bad"
    private val breakingBadItem = Item(breakingBadName, 7.00,ItemCategories.VIDEOS)
    private val breakingBadQuantity = 5
    private val breakingBadItems = BasketItem(breakingBadItem,breakingBadQuantity)

    private val hobbitName = "The Hobbit"
    private val hobbitItem = Item(hobbitName, 5.00,ItemCategories.BOOKS)
    private val hobbitQuantity = 2
    private val hobbitItems = BasketItem(hobbitItem,hobbitQuantity)

    private val dopaId = "dopa@etermax.com"
    private val bonzoId = "bonzo@etermax.com"

    private val dopaData = AddItem.ActionData(dopaId,breakingBadName,breakingBadQuantity)
    private val anotherDopaData = AddItem.ActionData(dopaId,hobbitName,hobbitQuantity)
    private val bonzoData = AddItem.ActionData(bonzoId,"Breaking Bad",breakingBadQuantity)
    private val dopaThreeHobbitBooksData = AddItem.ActionData(dopaId,hobbitName,3)


    @Before
    fun setUp() {
        shoppingBasketRepository = InMemoryShoppingBasketRepository()
        itemsRepository = InMemoryItemRepository()
        itemsRepository.add(breakingBadItem)
        itemsRepository.add(hobbitItem)
        clockService = TestClockService(NOW)
        availableDiscount = Discounts(DiscountFactory.create())
        addItem = AddItem(shoppingBasketRepository, clockService,itemsRepository,availableDiscount)
        itemsStock = InMemoryItemStock()
        itemsStock.addStock(hobbitName,100)
        itemsStock.addStock(breakingBadName,100)
        purchaseItems = PurchaseItems(itemsStock)
    }

    @Test
    fun `add an item should create a shopping basket`() {
        whenAddsAnItem()
        thenShoppingBasketIsCreated()
    }

    @Test
    fun `add an item should create a shopping basket with a creation date`() {
        whenAddsAnItem()
        thenShoppingBasketIsCreatedWithCreationDate()
    }

    @Test
    fun `add an item inside the shopping basket`() {
        whenAddsAnItem()
        thenItemIsInBasket()
    }

    @Test
    fun `add more than one item`() {
        givenAnExistingShoppingBasket()
        whenAddsAnotherItem()
        thenTwoItemsAreInBasket()
    }

    @Test
    fun `should create the shopping basket with an UserId`() {
        whenAddsAnItem()
        thenTheUserShoppingBasketIsCreatedWithHerItems()
    }

    @Test
    fun `should allow multiple shopping basket users`() {
        whenTwoUsersAddItems()
        thenTheUsersAndTheirBasketsAreFound()
    }

    @Test
    fun `should have the total amount of the basket`() {
        whenAddsAnItem()
        whenAddsAnotherItem()
        thenTheTotalShoppingBasketPriceIsCalculated()
    }

    @Test
    fun `should have item categories`() {
        whenAddsThreeHobbitBooks()
        thenHasCategories()
    }

    @Test
    fun `should apply 10 percentage of discount when 3 or more books in the basket`() {
        whenAddsThreeHobbitBooks()
        thenTenPercentageIsApplied()
    }

    @Test
    fun `should apply 20 percentage of discount when at least there are 1 book and 1 video in the basket`() {
        whenAddsThreeHobbitBooks()
        whenAddsOneMore()
        thenTwentyPercentageIsApplied()
    }

    private fun whenAddsThreeHobbitBooks() {
        addItem(dopaThreeHobbitBooksData)
    }

    @Test
    fun `should continue the purchase process when items are in stock`() {
        val basketItems = BasketItems(mutableListOf(BasketItem(breakingBadItem, breakingBadQuantity)))
        val actualShoppingBasket = ShoppingBasket(dopaId, basketItems, availableDiscount)
        purchaseItems(actualShoppingBasket)
    }

    @Test(expected = InsufficientStockException::class)
    fun `should throw an error when items are not in stock`() {
        val basketItems = BasketItems(mutableListOf(BasketItem(breakingBadItem, 101)))
        val actualShoppingBasket = ShoppingBasket(dopaId, basketItems, availableDiscount)
        purchaseItems(actualShoppingBasket)
    }

    @Test
    fun `should decrease stock with item is purchased`() {
        val basketItems = BasketItems(mutableListOf(BasketItem(breakingBadItem, 50)))
        val actualShoppingBasket = ShoppingBasket(dopaId, basketItems, availableDiscount)
        purchaseItems(actualShoppingBasket)

        val bonzoBasketItems = BasketItems(mutableListOf(BasketItem(breakingBadItem, 50)))
        val bonzoActualShoppingBasket = ShoppingBasket(bonzoId, bonzoBasketItems, availableDiscount)
        purchaseItems(bonzoActualShoppingBasket)
    }

    @Test
    fun `should create an Order with Order Id when purchase`() {
        val expectedOrderId = 1
        assertEquals(1,expectedOrderId)
    }

    private fun givenAnExistingShoppingBasket() {
        val basketItems = BasketItems(mutableListOf(BasketItem(breakingBadItem, breakingBadQuantity)))
        val existingShoppingBasket = ShoppingBasket(dopaId, basketItems, availableDiscount)
        shoppingBasketRepository.add(existingShoppingBasket)
    }

    private fun whenAddsAnotherItem() {
        addItem(anotherDopaData)
    }

    private fun whenAddsAnItem() {
        whenAddsOneMore()
    }

    private fun whenTwoUsersAddItems() {
        whenAddsOneMore()
        addItem(anotherDopaData)
        addItem(bonzoData)
    }

    private fun whenAddsOneMore() {
        addItem(dopaData)
    }

    private fun thenItemIsInBasket() {
        val expectedItems = BasketItems(mutableListOf(BasketItem(breakingBadItem, breakingBadQuantity)))
        val actualBasketItems = shoppingBasketRepository.find(dopaId)?.basketItems
        assertEquals(expectedItems, actualBasketItems)
    }

    private fun thenShoppingBasketIsCreated() {
        val savedShoppingBasket = shoppingBasketRepository.find(dopaId)
        assertNotNull(savedShoppingBasket)
    }

    private fun thenShoppingBasketIsCreatedWithCreationDate() {
        val shoppingBasket = shoppingBasketRepository.find(dopaId)
        val repositoryCreationDate = shoppingBasket?.getCreationDate()
        assertEquals(NOW,repositoryCreationDate)
    }

    private fun thenTheUserShoppingBasketIsCreatedWithHerItems() {
        val userShoppingBasket = shoppingBasketRepository.find(dopaId)
        val expectedAddedItems = BasketItems(mutableListOf(BasketItem(breakingBadItem, breakingBadQuantity)))
        val actualAddedItems = shoppingBasketRepository.find(dopaId)?.basketItems
        assertEquals(expectedAddedItems,actualAddedItems)
        assertNotNull(userShoppingBasket)
    }

    private fun thenTheUsersAndTheirBasketsAreFound() {
        val userShoppingBasket = shoppingBasketRepository.find(dopaId)
        val anotherUserShoppingBasket = shoppingBasketRepository.find(bonzoId)
        assertNotNull(userShoppingBasket)
        assertEquals(dopaData.userId, userShoppingBasket?.userId)
        assertEquals(BasketItems(mutableListOf(breakingBadItems, hobbitItems)), userShoppingBasket?.basketItems)
        assertNotNull(anotherUserShoppingBasket)
        assertEquals(bonzoData.userId, anotherUserShoppingBasket?.userId)
        assertEquals(BasketItems(mutableListOf(breakingBadItems)), anotherUserShoppingBasket?.basketItems)
    }

    private fun thenTheTotalShoppingBasketPriceIsCalculated() {
        val expectedTotalAmount = (breakingBadItem.price*dopaData.quantity) + (hobbitItem.price*anotherDopaData.quantity)
        val shoppingBasket = shoppingBasketRepository.find(dopaId)
        val basketTotalAmount = shoppingBasket?.basketItems?.getTotalBasketPrice()
        assertEquals(expectedTotalAmount,basketTotalAmount)
    }

    private fun thenTwoItemsAreInBasket() {
        val expectedItems = BasketItems(mutableListOf(breakingBadItems, hobbitItems))
        val actualItems = shoppingBasketRepository.find(dopaId)?.basketItems
        assertEquals(expectedItems,actualItems)
    }

    private fun thenHasCategories() {
        val actualShoppingBasket = shoppingBasketRepository.find(dopaId)
        val actualItemCategory = actualShoppingBasket!!.basketItems.basketItems.first().item.category
        assertEquals("BOOKS", actualItemCategory.toString())
    }

    private fun thenTenPercentageIsApplied() {
        val expectedPriceWithDiscount = (hobbitItem.price * 3) * 0.9
        val actualShoppingBasket = shoppingBasketRepository.find(dopaId)
        assertEquals(expectedPriceWithDiscount, actualShoppingBasket?.getFinalPrice())
    }

    private fun thenTwentyPercentageIsApplied() {
        val expectedPriceWithDiscount = ((hobbitItem.price * 3) + (breakingBadItem.price * 5)) * 0.8
        val actualShoppingBasket = shoppingBasketRepository.find(dopaId)
        assertEquals(expectedPriceWithDiscount, actualShoppingBasket?.getFinalPrice())
    }

    companion object {
        val NOW = LocalDateTime.now()
    }
}


